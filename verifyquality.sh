#!/bin/bash
#
# A bash script that uses mplayer to get the height of the
# video in pixels and verifies it with a tag in the file name.
#
# Holden Lewis
#

TARGET="$PWD/$1"
FILE_COUNTER=1
TOLERANCE=0 # Default tolerance is 0.
FILES="$(find "$TARGET" -type f)"
TOTAL_FILES="$(echo "$FILES" | wc -l)"

# If the variable is set.
if [ -n "$2" ]; then
    TOLERANCE=$2
fi

IFS=$'\n'
for FILE in $FILES; do
    # Gets the value for the line containing the video height in pixels.
    ID_VIDEO_HEIGHT="$(mplayer -vo null -ao null -identify -frames 0 "$FILE" \
            2> /dev/null | sed -e '/^ID_VIDEO_HEIGHT/ !d' -e 's/^ID_VIDEO_HEIGHT=//')"

    # Match the part of the file name with the quality
    # and remove all extraneous characters.
    STATED_QUALITY="$(echo $FILE | grep -o '\[[0-9]\{3,4\}p\]' | tr -d "[]p" )"
    #echo "Stated Quality: $STATED_QUALITY"
    #echo "ID: $ID_VIDEO_HEIGHT"
    # Can't just check if it is not set because it will
    # always be set to at least an empty string.
    if [[ $STATED_QUALITY -eq "" ]]; then
        echo "$FILE has not set a quality. It has $ID_VIDEO_HEIGHT"
    # If they don't match, then print out a message.
    elif [ $STATED_QUALITY -ne $ID_VIDEO_HEIGHT ]; then
        DIFFERENCE="$(expr $STATED_QUALITY - $ID_VIDEO_HEIGHT | tr -d "-")"
        if (( $DIFFERENCE  > $TOLERANCE )); then
            echo "$FILE has $ID_VIDEO_HEIGHT but reported $STATED_QUALITY"
        fi
    fi

done
